import React, { useEffect, useState } from 'react';
import './App.css';
import 'antd/dist/reset.css';
import { Button, Col, Row } from 'antd';
import { web3auth } from './web3RPC';
import Web3 from 'web3';

function App() {
  const [account, setAccount] = useState("");
  const web3AuthInitial = async () => {
    await web3auth.initModal();
  }
  useEffect(() => {
    web3AuthInitial();
  }, []);
  useEffect(() => {
    if (web3auth.provider) {
      const web3 = new Web3(web3auth.provider as any);
      // Get user's Ethereum public address
      web3.eth.getAccounts().then(accounts => {
        setAccount(accounts[0])
      })
    }
  }, [web3auth.provider]);
  return (
    <div className="App">
      <Row>
        <Col>
          {!web3auth.provider && (
            <Button onClick={async () => {
              await web3auth.connect();
            }}>Login</Button>
          )}
          {web3auth.provider && (
            <h3>{account}</h3>
          )}
        </Col>
        <Col>
          <Button onClick={async () => {
            const privateKey = await web3auth.provider?.request({
              method: "eth_private_key"
            });
            alert(privateKey);
          }}>Private key</Button>
        </Col>
      </Row>

    </div>
  );
}

export default App;
