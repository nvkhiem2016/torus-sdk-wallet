import { Web3Auth } from "@web3auth/modal";

export const web3auth = new Web3Auth({
  clientId: "BHIePNZKQfiwpzAVH0djpvbXxNl7S_Fft6dZRvu030k9LML_bdWBHIfBikkbKA9o7rAx03nI9M8qIUK8YdIEjkw", // Get your Client ID from Web3Auth Dashboard
  chainConfig: {
    chainNamespace: "eip155",
    chainId: "0x1",
    rpcTarget: "https://rpc.ankr.com/eth", // This is the mainnet RPC we have added, please pass on your own endpoint while creating an app
  },
});